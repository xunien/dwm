pkgname=dwm-git
_pkgname=dwm
pkgver=6.2.r5.gf09418b
pkgrel=1
pkgdesc="A dynamic window manager for X"
url="http://dwm.suckless.org"
arch=('i686' 'x86_64')
license=('MIT')
options=(zipman)
depends=('libx11' 'libxinerama' 'libxft')
makedepends=('git')
install=dwm.install
provides=('dwm')
conflicts=('dwm')

_patches=(dwm-systray-f09418b.diff)

source=(dwm.desktop
        "$_pkgname::git+http://git.suckless.org/dwm"
        config.h
        "${_patches[@]}")

_patches_md5sums=('4f2fe4e7d8535cc2f96de6d56952fabd') # dwm-systray-f09418b.diff

md5sums=('939f403a71b6e85261d09fc3412269ee'
         'SKIP' # dwm-git
         'SKIP' # config.h
         "${_patches_md5sums[@]}")

pkgver(){
  cd $_pkgname
  git describe --long --tags | sed -E 's/([^-]*-g)/r\1/;s/-/./g'
}

prepare() {
  cd $_pkgname
  if [[ -f "$srcdir/config.h" ]]; then
    cp -fv "$srcdir/config.h" config.h
  fi

  for file in "${_patches[@]}"; do
    echo "Applying patch $(basename $file)..."
    git apply "$srcdir/$(basename ${file})"
  done
}

build() {
  cd $_pkgname
  make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  cd $_pkgname
  make PREFIX=/usr DESTDIR="$pkgdir" install
  install -m644 -D LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -m644 -D README "$pkgdir/usr/share/doc/$pkgname/README"
  install -m644 -D ../dwm.desktop "$pkgdir/usr/share/xsessions/dwm.desktop"
}

# vim:set ts=2 sw=2 et:
